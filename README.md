#1)Installer Node + npm
https://openclassrooms.com/courses/des-applications-ultra-rapides-avec-node-js/installer-node-js

#2)Configuer Votre Compte Auth0

#3)Creation D'un projet (Single Page App)
	Lors de la creation , Ajouter le Callback Url (par exemple http://localhost:3000/callback)

#4)Creation d'un Api 
Dans ce Cas on a créer un API Express

#5)Remplacer les Dans app.js cette parties avec les données de votre compte Auth0

angularAuth0Provider.init({ // il faut mettre les données de votre compte Auth0 ici
        clientID :'32P1fxaWxVfoag0kAgCqfPuixeQqwuV6',
        domain : 'ghailene.auth0.com',
        responseType : 'token id_token',
        redirectUri : 'http://localhost:3000/callback',
        scope : 'openid profile',
        audience: 'https://ghailene.com/api'
      });

#6)cd angularjs-auth
	npm i --save angular @uirouter/angularjs auth0-js angular-auth0 bootstrap

	node server.js (indispensable pour la redirection du lien de callback)

7)visiter localhost:3000 

Pour l'API node

#8-a)cd angular-auth-server

	npm i express express-jwt jwks-rsa cors --save


	node server.js

Pour l'Api php

#8-b)cd angularjs-auth-serverphp

composer install

php -S localhost:8080

#9)Tester avec Insomnia

installer insomnia (https://insomnia.rest/download/)

cliquer sur 'New Request' , Choisir Get , aprés écrire http://localhost:8080/api/private ==> Réponse 401 Unauthorized

Authentifier avec votre application

Dans la console , allez vers Application , localStorage et copier le access_token

Choisir dans insomnia Auth puis Bearer et coller le access_token dans TOKEN

refaire la requette Get et ca marche 




