(function(){//Service d'authentification Auth0
  angular.module('app').service('authService',authService);
  authService.$inject = ['$state','angularAuth0','$timeout'];
  function authService($state,angularAuth0,$timeout){
    function login(){ // la fonction de login par Auth0
      angularAuth0.authorize();

    }
    function handleAuthentication(){ // une fois l'utilisateur est authentifié , on stocke le jeton +
                                    //données personnelles dans le local Storage afain d'établir une session locale
      angularAuth0.parseHash(function(err,authResult){
        if(authResult && authResult.accessToken && authResult.idToken){
          setSession(authResult);
          $timeout(function(){
            $state.go('home') // une fois authentifié , on redirect le user vers /home
          })
        }
      });
    }
    function setSession(authResult){//Etablir la session
      var expiresAt = JSON.stringify(
        (authResult.expiresIn * 1000) + new Date().getTime() // temps d'expiration de Jeton
      );
      var profile = {
        name : authResult.idTokenPayload.name,
        nickname : authResult.idTokenPayload.nickname

      }
      localStorage.setItem('access_token',authResult.accessToken);//stocker les informations de jeton dans les cookies
      localStorage.setItem('id_token',authResult.idToken);
      localStorage.setItem('expires_at',expiresAt);
      localStorage.setItem('profile',JSON.stringify(profile));


    }
    function logout(authResult){//logout , on vide le localStorage
      localStorage.removeItem('access_token');
      localStorage.removeItem('id_token');
      localStorage.removeItem('expires_at');
    }
    function isAuthenticated(){ // verifier si un utilisateur est authentifié
      var expiresAt = JSON.parse(localStorage.getItem('expires_at'));
      return new Date().getTime() < expiresAt;
    }
    return{
      login: login,
      handleAuthentication : handleAuthentication,
      logout : logout,
      isAuthenticated : isAuthenticated
    };
  }

})();
