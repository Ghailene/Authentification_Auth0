(function(){
  angular
    .module('app',['auth0.auth0','ui.router','angular-jwt'])
    .config(config);

    config.$inject=[
      '$stateProvider',
      '$locationProvider',
      '$urlRouterProvider',
      'angularAuth0Provider',//Auth0 configuration
      'jwtOptionsProvider', // JWt configuration
      '$httpProvider'
    ];


    function config($stateProvider,$locationProvider, $urlRouterProvider, angularAuth0Provider,jwtOptionsProvider,$httpProvider){
      $stateProvider.state('home',{
        url : '/',
        controller:'HomeController',
        templateUrl : 'app/home/home.html',
        controllerAs : 'vm'
      })
      .state('callback',{
        url : '/callback',
        controller : 'CallbackController',
        templateUrl : 'app/callback/callback.html',
        controllerAs : 'vm'
      })
      .state('profile',{
        url : '/profile',
        controller : 'ProfileController',
        templateUrl : 'app/profile/profile.html',
        controllerAs : 'vm'
      });
      angularAuth0Provider.init({ // il faut mettre les données de votre compte Auth0 ici
        clientID :'32P1fxaWxVfoag0kAgCqfPuixeQqwuV6',
        domain : 'ghailene.auth0.com',
        responseType : 'token id_token',
        redirectUri : 'http://localhost:3000/callback',
        scope : 'openid profile',
        audience: 'https://ghailene.com/api'
      });
      jwtOptionsProvider.config({
        tokenGetter : function(){
          return localStorage.getItem('access_token');//recuperation de jeton d'accées
        },
        whiteListedDomains : ['localhost,alwaysdata'] // ici on ajoute notre serveur local au list des endpoints qui peuvent utiliser le jeton d'accées
      });
      $httpProvider.interceptors.push('jwtInterceptor');//injecter le jeton dans le requettes http
      $urlRouterProvider.otherwise('/');
      $locationProvider.hashPrefix('');

      $locationProvider.html5Mode(true);



    }
})();
