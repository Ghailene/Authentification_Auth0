<?php
require __DIR__ . '/vendor/autoload.php';
use josegonzalez\Dotenv\Loader;
require __DIR__ . '/src/Main.php';
// Read .env
try {
  $dotenv = new Dotenv\Dotenv(__DIR__);
  $dotenv->load();
} catch(InvalidArgumentException $ex) {
  // Ignore if no dotenv
}

$app = new \App\Main();

// Create Router instance
$router = new \Bramus\Router\Router();

// Activate CORS
function sendCorsHeaders() {
  header("Access-Control-Allow-Origin: *");
  header("Access-Control-Allow-Headers: Authorization");
  header("Access-Control-Allow-Methods: GET,HEAD,PUT,PATCH,POST,DELETE");
}

$router->options('/.*', function() {
    sendCorsHeaders();
});

sendCorsHeaders();
// Check JWT on private routes
$router->before('GET', '/api/private.*', function() use ($app) {

  $requestHeaders = apache_request_headers();

  if (!isset($requestHeaders['authorization']) && !isset($requestHeaders['Authorization'])) {
    header('HTTP/1.0 401 Unauthorized');
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode(array("message" => "No token provided."));
    exit();
  }

  $authorizationHeader = isset($requestHeaders['authorization']) ? $requestHeaders['authorization'] : $requestHeaders['Authorization'];

  if ($authorizationHeader == null) {
    header('HTTP/1.0 401 Unauthorized');
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode(array("message" => "No authorization header sent."));
    exit();
  }

  $authorizationHeader = str_replace('bearer ', '', $authorizationHeader);
  $token = str_replace('Bearer ', '', $authorizationHeader);

  try {
    $app->setCurrentToken($token);
  }
  catch(\Auth0\SDK\Exception\CoreException $e) {
    header('HTTP/1.0 401 Unauthorized');
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode(array("message" => $e->getMessage()));
    exit();
  }
});
$router->get('/api/public', function() use ($app){
  header('Content-Type: application/json; charset=utf-8');
  echo json_encode($app->publicEndpoint());
});

$router->get('/api/private', function() use ($app){
  header('Content-Type: application/json; charset=utf-8');
  echo json_encode($app->privateEndpoint());
});

$router->run();
