//notre API express
var express = require('express');
var app = express();
var jwt = require('express-jwt');
var jwks = require('jwks-rsa');
var cors = require('cors')

var port = process.env.PORT || 8080;

var jwtCheck = jwt({//assurer que le jeton est verifié
    secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: "https://ghailene.auth0.com/.well-known/jwks.json"//notre public key
    }),
    audience: 'https://ghailene.com/api',
    issuer: "https://ghailene.auth0.com/",
    algorithms: ['RS256']
});
app.use(cors())
app.use(jwtCheck);// etre sur que chaque requete a une jeton d'autorisation

app.get('/authorized', function (req, res) {
  res.json({message : 'This is secure '});
});

app.listen(port);
console.log('server running');
